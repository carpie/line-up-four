import styled, { keyframes } from 'styled-components';

import boardCellMask from './board-cell.svg';

const dropAnimation = props => keyframes`
  0% { transform: translateY(-${(props.row + 1) * 3}rem); }
  100% { transform: translateY(0); }
`;

const dropAnimationMedium = props => keyframes`
  0% { transform: translateY(-${(props.row + 1) * 5}rem); }
  100% { transform: translateY(0); }
`;

const dropAnimationLarge = props => keyframes`
  0% { transform: translateY(-${(props.row + 1) * 6}rem); }
  100% { transform: translateY(0); }
`;

const pulseAnimation = keyframes`
  0%, 100% { opacity: 0.4; }
  50% { opacity: 0.8; }
`;

export const Board = styled.div`
  position: relative;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, .2);
`;

export const BoardCell = styled.div`
  width: var(--cell-width);
  height: var(--cell-width);
  margin: var(--cell-margin);
  border-radius: 50%;
  position: relative;
`;

export const BoardOverlay = styled.div`
  position: absolute;
  top: 0; left: 0;
  height: 100%; width: 100%;
`;

export const BoardRow = styled.div`
  display: flex;
`;

export const Button = styled.button`
  border: 1px solid darkgray;
  padding: 0.5rem 3rem;
  font-size: var(--btn-font-size);
  background-color: #22aed1;
  color: white;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, .5);

  &:active {
    position: relative;
    top: 1px; left: 1px;
    box-shadow: 1px 1px 4px rgba(0, 0, 0, .4);
  }
`;

export const Content = styled.article`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

export const GamePiece = styled.div`
  width: var(--cell-width, 2rem);
  height: var(--cell-width, 2rem);
  margin: var(--cell-margin, 0.5rem);
  border-radius: 50%;
  background-color: ${props => props.color === 1 ? 'red' : 'black'};
  animation: ${dropAnimation} 0.${props => props.row + 1}s ease-in;

  @media(min-width: 600px) {
    animation: ${dropAnimationMedium} 0.${props => props.row + 1}s ease-in;
  }

  @media(min-width: 768px) {
    animation: ${dropAnimationLarge} 0.${props => props.row + 1}s ease-in;
  }
`;

export const DropCell = styled.button`
  border: none;
  width: var(--cell-width, 2rem);
  height: var(--cell-width, 2rem);
  margin: var(--cell-margin, 0.5rem);
  background-color: #eee;
  border-radius: 50%;

  &:hover, &:focus {
    background-color: ${props => props.color};
    animation: ${pulseAnimation} 1.4s linear infinite;
  }
`;

export const DropZone = styled.div`
  display: flex;
`;

export const FourApp = styled.main`
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  height: 100vh;
  min-width: 22rem;

  --cell-width: 2.5rem;
  --cell-margin: 0.25rem;
  --overlay-width: 3rem;
  --header-font-size: 1.6rem;
  --header-padding: 0.8rem;
  --btn-font-size: 1.2rem;

  @media(min-width: 600px) {
    --cell-width: 4rem;
    --cell-margin: 0.5rem;
    --overlay-width: 5rem;
    --header-font-size: 2rem;
    --header-padding: 1rem;
    --btn-font-size: 1.8rem;
  }

  @media(min-width: 768px) {
    --cell-width: 5rem;
    --cell-margin: 0.5rem;
    --overlay-width: 6rem;
    --header-font-size: 3rem;
    --header-padding: 1.5rem;
    --btn-font-size: 2.4rem;
  }
`;

export const Header = styled.header`
  position: relative;
  background-color: #016fb9;
  color: white;
  font-size: var(--header-font-size);
  padding: var(--header-padding);
  margin-bottom: 2rem;
  width: 100%;
  text-transform: capitalize;
`;

export const OverlayCell = styled.div`
  width: var(--overlay-width, 3rem);
  height: var(--overlay-width, 3rem);
  background-image: url(${boardCellMask});
  background-size: cover;
`;

export const PlayAgain = styled.div`
  height: var(--overlay-width);
  overflow: hidden;
`;

export const StartButton = styled(Button)`
  margin-top: 4rem;
`;

export const VersionArea = styled.div`
  position: absolute;
  bottom: 0.5rem;
  right: 0.5rem;
  font-size: 0.8rem;

  display: flex;
  flex-direction: column;

  & > a {
    color: white;
  }
`;
