// Most of the logic is tested by the App integration tests.
// These tests test some specific gameLogic corner cases that are easier to test here.
import { didLastPlayWin } from './gameLogic';

const generateEmptyBoard = () => {
  const board = new Array(6);
  for (let row = 0; row < 6; row +=1) {
    board[row] = [0, 0, 0, 0, 0, 0, 0];
  }
  return board;
}

const placeFourVertical = (board, row, col) => {
  for (let i = row; i < (row + 4); i += 1) {
    board[i][col] = 1;
  }
  return board;
};

const placeFourHorizontal = (board, row, col) => {
  for (let i = col; i < (col + 4); i += 1) {
    board[row][i] = 1;
  }
  return board;
};

const placeFourForwardDiagonal = (board, row, col) => {
  for (let i = col, j = row; (i < (col + 4)) && (j > (row - 4)); i += 1, j -= 1) {
    board[j][i] = 1;
  }
  return board;
};

const placeFourReverseDiagonal = (board, row, col) => {
  for (let i = col, j = row; (i < (col + 4)) && (j < (row + 4)); i += 1, j += 1) {
    board[j][i] = 1;
  }
  return board;
};

const dumpBoard = board => {
  const rows = [];
  for (let i = 0; i < 6; i += 1) {
    rows.push(board[i].join(' '));
  }
  return rows.join('\n');
};

// Custom matcher to dump the board out on failure
expect.extend({
  toDetectAWin({ result, board, lastPlayed }) {
    if (!result) {
      return {
        message: () => `${result}: ${lastPlayed}\n${dumpBoard(board)}`,
        pass: false
      }
    }
    return {
      message: () => 'passed',
      pass: true
    }
  }
});

describe('didLastPlayWin', () => {
  // Probably overkill, but here we test that all possible winning patterns are deteted
  it('detects all winning vertical patterns', () => {
    for (let col = 0; col < 7; col += 1) {
      for (let row = 0; row < 3; row += 1) {
        const board = placeFourVertical(generateEmptyBoard(), row, col);
        for (let last = row; last < (row + 4); last += 1) {
          const results = {
            result: didLastPlayWin({ board, lastPlayed: [last, col] }),
            board,
            lastPlayed: [last, col]
          };
          expect(results).toDetectAWin();
        }
      }
    }
  });

  it('detects all winning horizontal patterns', () => {
    for (let col = 0; col < 4; col += 1) {
      for (let row = 0; row < 6; row += 1) {
        const board = placeFourHorizontal(generateEmptyBoard(), row, col);
        for (let last = col; last < (col + 4); last += 1) {
          // console.log('checking', row, last);
          // console.log(dumpBoard(board));
          const results = {
            result: didLastPlayWin({ board, lastPlayed: [row, last] }),
            board,
            lastPlayed: [row, last]
          };
          expect(results).toDetectAWin();
        }
      }
    }
  });

  it('detects all winning forward diagnonal patterns', () => {
    for (let col = 0; col < 4; col += 1) {
      for (let row = 3; row < 6; row += 1) {
        const board = placeFourForwardDiagonal(generateEmptyBoard(), row, col);
        for (let lastCol = col, lastRow = row;
          (lastCol < (col + 4)) && (lastRow > (row - 4));
          lastCol += 1, lastRow -= 1) {
          const results = {
            result: didLastPlayWin({ board, lastPlayed: [lastRow, lastCol] }),
            board,
            lastPlayed: [lastRow, lastCol]
          };
          expect(results).toDetectAWin();
        }
      }
    }
  });

  it('detects all winning reverse diagnonal patterns', () => {
    for (let col = 0; col < 4; col += 1) {
      for (let row = 0; row < 3; row += 1) {
        const board = placeFourReverseDiagonal(generateEmptyBoard(), row, col);
        for (let lastCol = col, lastRow = row;
          (lastCol < (col + 4)) && (lastRow < (row + 4));
          lastCol += 1, lastRow += 1) {
          const results = {
            result: didLastPlayWin({ board, lastPlayed: [lastRow, lastCol] }),
            board,
            lastPlayed: [lastRow, lastCol]
          };
          expect(results).toDetectAWin();
        }
      }
    }
  });

  it('does not detect empty patterns as wins', () => {
    const board = generateEmptyBoard();
    const results = {
      result: didLastPlayWin({ board, lastPlayed: [1, 2] }),
      board,
      lastPlayed: [1, 2]
    };
    expect(results).not.toDetectAWin();
  });
});
