import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import App from './App';

describe('on startup', () => {
  let getByText;
  beforeEach(() => {
    ({ getByText } = render(<App />));
  });

  it('renders a header', () => {
    const headerTitle = getByText(/Line Up Four/i);
    expect(headerTitle).toBeInTheDocument();
  });

  it('renders the start button', () => {
    const headerTitle = getByText(/Start/i);
    expect(headerTitle).toBeInTheDocument();
  });
});


describe('on start', () => {
  let getByTestId;
  let getByText;
  beforeEach(() => {
    ({ getByTestId, getByText } = render(<App />));
    fireEvent.click(getByText(/Start/i));
  });

  it('renders the gameboard', () => {
    const gameboard = getByTestId('gameboard');
    expect(gameboard).toBeInTheDocument();
  });

  it('renders a dropzone to allow game pieces to drop', () => {
    const dropzone = getByTestId('dropzone');
    expect(dropzone).toBeInTheDocument();
  });
});


describe('during gameplay', () => {
  let container;
  let findByText;
  let getByTestId;
  let getByText;

  // Some helpers to make the tests more readable
  const getDropArea = col => getByTestId('dropzone').querySelector(`button:nth-child(${col})`);
  const getRow = (container, row) => container.querySelector(`[data-testid=gameboard] > div:nth-child(${row})`);
  const getCell = (container, col) => container.querySelector(`div:nth-child(${col})`);
  beforeEach(() => {
    ({ container, findByText, getByTestId, getByText } = render(<App />));
  });

  it('allows dropping pieces in each column', () => {
    fireEvent.click(getByText(/Start/i));

    const row = getRow(container, 6);
    expect(getCell(row, 1)).not.toHaveStyle('background-color: red');
    fireEvent.click(getDropArea(1));
    expect(getCell(row, 1)).toHaveStyle('background-color: red');

    expect(getCell(row, 2)).not.toHaveStyle('background-color: black');
    fireEvent.click(getDropArea(2));
    expect(getCell(row, 2)).toHaveStyle('background-color: black');

    expect(getCell(row, 3)).not.toHaveStyle('background-color: red');
    fireEvent.click(getDropArea(3));
    expect(getCell(row, 3)).toHaveStyle('background-color: red');

    expect(getCell(row, 4)).not.toHaveStyle('background-color: black');
    fireEvent.click(getDropArea(4));
    expect(getCell(row, 4)).toHaveStyle('background-color: black');

    expect(getCell(row, 5)).not.toHaveStyle('background-color: red');
    fireEvent.click(getDropArea(5));
    expect(getCell(row, 5)).toHaveStyle('background-color: red');

    expect(getCell(row, 6)).not.toHaveStyle('background-color: black');
    fireEvent.click(getDropArea(6));
    expect(getCell(row, 6)).toHaveStyle('background-color: black');

    expect(getCell(row, 7)).not.toHaveStyle('background-color: red');
    fireEvent.click(getDropArea(7));
    expect(getCell(row, 7)).toHaveStyle('background-color: red');
  });

  it('does not allow dropping a piece in a full column', () => {
    fireEvent.click(getByText(/Start/i));

    for (let i = 1; i < 7; i += 1) {
      fireEvent.click(getDropArea(1));
    }
   
    let row = getRow(container, 1);
    expect(getCell(row, 1)).toHaveStyle('background-color: black');

    // At this point, the drop piece should be red
    // Attemping to drop a red piece on the full column should not cause red
    // to lose a turn
    fireEvent.click(getDropArea(1));
    // So now, if we drop in the next column, the piece should be red
    fireEvent.click(getDropArea(2));
    row = getRow(container, 6);
    expect(getCell(row, 2)).toHaveStyle('background-color: red');
  });

  it('detects four reds in a row as a win for red', async () => {
    fireEvent.click(getByText(/Start/i));

    fireEvent.click(getDropArea(7));
    fireEvent.click(getDropArea(6));
    fireEvent.click(getDropArea(7));
    fireEvent.click(getDropArea(6));
    fireEvent.click(getDropArea(7));
    fireEvent.click(getDropArea(6));
    // Right now we're still in play, so the header is still there
    expect(getByText(/Line Up Four/i)).toBeInTheDocument();
    // Now red will win
    fireEvent.click(getDropArea(7));
    expect(await findByText(/Red wins/i)).toBeInTheDocument();
    // Play again button is available
    expect(getByText(/Play again/i)).toBeInTheDocument();
  });

  it('detects four blacks in a row as a win for black', async () => {
    fireEvent.click(getByText(/Start/i));

    fireEvent.click(getDropArea(2));
    fireEvent.click(getDropArea(1));
    fireEvent.click(getDropArea(3));
    fireEvent.click(getDropArea(2));
    fireEvent.click(getDropArea(3));
    fireEvent.click(getDropArea(3));
    fireEvent.click(getDropArea(4));
    fireEvent.click(getDropArea(4));
    fireEvent.click(getDropArea(4));
    // Right now we're still in play, so the header is still there
    expect(getByText(/Line Up Four/i)).toBeInTheDocument();
    // Now black will win
    fireEvent.click(getDropArea(4));
    expect(await findByText(/Black wins/i)).toBeInTheDocument();
    // Play again button is available
    expect(getByText(/Play again/i)).toBeInTheDocument();
  });

  it('declares a draw if board is full with no winner', async () => {
    fireEvent.click(getByText(/Start/i));

    expect(getByText(/Line Up Four/i)).toBeInTheDocument();
    for (let i = 0; i < 6; i += 1) {
      // Drop in such a manner that no game piece has four in a row
      for (let j = 1; j < 8; j+=2) {
        fireEvent.click(getDropArea(j));
      }
      for (let j = 2; j < 8; j+=2) {
        fireEvent.click(getDropArea(j));
      }
    }
    expect(await findByText(/The game is a draw/i)).toBeInTheDocument();
    // Play again button is available
    expect(getByText(/Play again/i)).toBeInTheDocument();
  });

  it('starts over if play again pressed', async () => {
    fireEvent.click(getByText(/Start/i));

    expect(getByText(/Line Up Four/i)).toBeInTheDocument();
    for (let i = 0; i < 6; i += 1) {
      // Drop in such a manner that no game piece has four in a row
      for (let j = 1; j < 8; j+=2) {
        fireEvent.click(getDropArea(j));
      }
      for (let j = 2; j < 8; j+=2) {
        fireEvent.click(getDropArea(j));
      }
    }
    expect(await findByText(/The game is a draw/i)).toBeInTheDocument();
    // Play again button is available
    expect(getByText(/Play again/i)).toBeInTheDocument();
    fireEvent.click(getByText(/Play again/i));

    // The board should be clear
    for (let i = 1; i < 7; i += 1) {
      const row = getRow(container, i);
      for (let j = 1; j < 8; j+=2) {
        expect(getCell(row, 2)).not.toHaveStyle('background-color: red');
        expect(getCell(row, 2)).not.toHaveStyle('background-color: black');
      }
    }
  });
});
