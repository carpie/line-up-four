import React from 'react';

import { useMachine } from '@xstate/react';

import pkg from '../package.json';
import {
  FourApp,
  Board,
  BoardCell,
  BoardOverlay,
  BoardRow,
  Button,
  Content,
  DropCell,
  DropZone,
  GamePiece,
  Header,
  OverlayCell,
  PlayAgain,
  StartButton,
  VersionArea
} from './AppStyles';
import gameMachine from './gameMachine';

function App() {
  const [current, send] = useMachine(gameMachine, { devTools: true });
  const { board } = current.context;

  const onStart = () => send('START');
  const onDrop = (player, column) => {
    send('DROP_PIECE', { player, column });
  };

  const drawDropzone = () => {
    const cols = [0, 0, 0, 0, 0, 0, 0];
    const player = current.matches('playing.redsTurn') ? 'red' : 'black';
    if (current.matches('winner') || current.matches('draw')) {
      return <PlayAgain><Button onClick={onStart}>Play Again</Button></PlayAgain>;
    }
    return (
      <DropZone key="dropzone" data-testid="dropzone">
        {cols.map((_val, idx) => (
          <DropCell key={idx} color={player} onClick={() => onDrop(player, idx)} />
        ))}
      </DropZone>
    );
  };

  const drawBoardOverlay = board => {
    return (
      <BoardOverlay key="overlay">
        {board.map((row, ridx) => (
          <BoardRow key={ridx}>
            {row.map((col, cidx) => <OverlayCell key={cidx} />)}
          </BoardRow>
        ))}
      </BoardOverlay>
    );
  };

  const drawBoard = board => {
    const rows = board.map((row, ridx) => {
      return (
        <BoardRow key={ridx}>
          {row.map((col, cidx) => {
            const val = board[ridx][cidx];
            if (!val) {
              return <BoardCell key={cidx} />;
            }
            return <GamePiece key={cidx} row={ridx} color={board[ridx][cidx]} />;
          })}
        </BoardRow>
      );
    });

    return (
      <>
        {drawDropzone()}
        <Board data-testid="gameboard">
          {rows}
          {drawBoardOverlay(board)}
        </Board>
      </>
    );
  };

  return (
    <FourApp>
      <Header>
        {current.matches('winner') ? (
          <span>{current.value.winner} wins!</span>
        ) : (current.matches('draw') ? (
            <span>The game is a draw!</span>
          ) : (
            <span>Line Up Four!</span>
          )
        )}
        <VersionArea>
          <div>{pkg.version}</div>
          <a href="https://gitlab.com/carpie/line-up-four" target="_blank" rel="noopener noreferrer">
            Get the source!
          </a>
        </VersionArea>
      </Header>
      <Content>
        {current.matches('idle') ? (
          <StartButton onClick={onStart}>Start</StartButton>
        ) : (
          drawBoard(board)
        )}
      </Content>
    </FourApp>
  );
}

export default App;
