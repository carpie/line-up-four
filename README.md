# Line Up Four!

A simple turn-based strategy game in which two players attempt to line four of
the same color games pieces in a row.  This game was created as a demonstration
of releasing a frontend application on a Raspberry Pi Kubernetes cluster using
k3s.
